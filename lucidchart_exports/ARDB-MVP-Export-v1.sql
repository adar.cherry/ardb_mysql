-- MySQL dump 10.13  Distrib 8.0.29, for macos12 (x86_64)
--
-- Host: my-ardb-master.mysql.database.azure.com    Database: ardb_mvp
-- ------------------------------------------------------
-- Server version	5.7.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ar_business_need`
--

DROP TABLE IF EXISTS `ar_business_need`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ar_business_need` (
  `need_id` int(11) NOT NULL,
  `need_name` varchar(50) DEFAULT NULL,
  `description_need` text,
  PRIMARY KEY (`need_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ar_business_need`
--

LOCK TABLES `ar_business_need` WRITE;
/*!40000 ALTER TABLE `ar_business_need` DISABLE KEYS */;
/*!40000 ALTER TABLE `ar_business_need` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ar_feature`
--

DROP TABLE IF EXISTS `ar_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ar_feature` (
  `ar_feature_name` varchar(50) NOT NULL,
  `description_ar_feature` text,
  `ftype_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ar_feature_name`),
  KEY `ftype_name` (`ftype_name`),
  CONSTRAINT `ar_feature_ibfk_1` FOREIGN KEY (`ftype_name`) REFERENCES `type_feature` (`ftype_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ar_feature`
--

LOCK TABLES `ar_feature` WRITE;
/*!40000 ALTER TABLE `ar_feature` DISABLE KEYS */;
/*!40000 ALTER TABLE `ar_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ar_headset`
--

DROP TABLE IF EXISTS `ar_headset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ar_headset` (
  `headset_id` int(11) NOT NULL,
  `name_headset` varchar(50) DEFAULT NULL,
  `provider_name` varchar(50) DEFAULT NULL,
  `description_headset` text,
  `imageurl_headset` varchar(50) DEFAULT NULL,
  `htype_name` varchar(50) DEFAULT NULL,
  `release_year_headset` year(4) DEFAULT NULL,
  `price_average_headset` decimal(10,0) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `editing_id` int(11) DEFAULT NULL,
  `technical_features_headset` text,
  `pros_headset` text,
  `cons_headset` text,
  `ar_feature_name` varchar(50) DEFAULT NULL,
  `artifact_name` varchar(50) DEFAULT NULL,
  `relevance_editorrating_headset` int(11) DEFAULT NULL,
  `b2b_headset` tinyint(1) DEFAULT NULL,
  `software_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`headset_id`),
  KEY `editing_id` (`editing_id`),
  KEY `source_id` (`source_id`),
  KEY `provider_name` (`provider_name`),
  KEY `htype_name` (`htype_name`),
  KEY `feature_fk_2_idx` (`ar_feature_name`),
  KEY `artifact_fk_2_idx` (`artifact_name`),
  KEY `software_fk_2_idx` (`software_id`),
  CONSTRAINT `ar_headset_ibfk_1` FOREIGN KEY (`provider_name`) REFERENCES `headset_provider` (`provider_name`),
  CONSTRAINT `ar_headset_ibfk_2` FOREIGN KEY (`htype_name`) REFERENCES `type_headset` (`htype_name`),
  CONSTRAINT `artifact_fk_2` FOREIGN KEY (`artifact_name`) REFERENCES `usecase_artifact` (`artifact_name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `edit_fk_1` FOREIGN KEY (`editing_id`) REFERENCES `enter_editing` (`editing_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `feature_fk_2` FOREIGN KEY (`ar_feature_name`) REFERENCES `ar_feature` (`ar_feature_name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `software_fk_2` FOREIGN KEY (`software_id`) REFERENCES `development_software` (`software_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `source_fk_2` FOREIGN KEY (`source_id`) REFERENCES `enter_source` (`source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ar_headset`
--

LOCK TABLES `ar_headset` WRITE;
/*!40000 ALTER TABLE `ar_headset` DISABLE KEYS */;
/*!40000 ALTER TABLE `ar_headset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_division`
--

DROP TABLE IF EXISTS `business_division`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `business_division` (
  `division_name` varchar(50) NOT NULL,
  PRIMARY KEY (`division_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_division`
--

LOCK TABLES `business_division` WRITE;
/*!40000 ALTER TABLE `business_division` DISABLE KEYS */;
/*!40000 ALTER TABLE `business_division` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_process`
--

DROP TABLE IF EXISTS `business_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `business_process` (
  `prozess_id` int(11) NOT NULL,
  `name_prozess` varchar(50) DEFAULT NULL,
  `description_prozess` text,
  `division_name` varchar(50) DEFAULT NULL,
  `need_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`prozess_id`),
  KEY `need_id` (`need_id`),
  KEY `division_name` (`division_name`),
  CONSTRAINT `business_process_ibfk_1` FOREIGN KEY (`division_name`) REFERENCES `business_division` (`division_name`),
  CONSTRAINT `need_fk_1` FOREIGN KEY (`need_id`) REFERENCES `ar_business_need` (`need_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_process`
--

LOCK TABLES `business_process` WRITE;
/*!40000 ALTER TABLE `business_process` DISABLE KEYS */;
/*!40000 ALTER TABLE `business_process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `development_software`
--

DROP TABLE IF EXISTS `development_software`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `development_software` (
  `software_id` int(11) NOT NULL,
  `name_software` varchar(50) DEFAULT NULL,
  `description_software` text,
  `stype_name` varchar(50) DEFAULT NULL,
  `price_average_software` decimal(10,0) DEFAULT NULL,
  `relevance_editorrating_software` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`software_id`),
  KEY `source_id` (`source_id`),
  KEY `stype_name` (`stype_name`),
  CONSTRAINT `development_software_ibfk_1` FOREIGN KEY (`stype_name`) REFERENCES `type_software` (`stype_name`),
  CONSTRAINT `source_fk_1` FOREIGN KEY (`source_id`) REFERENCES `enter_source` (`source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `development_software`
--

LOCK TABLES `development_software` WRITE;
/*!40000 ALTER TABLE `development_software` DISABLE KEYS */;
/*!40000 ALTER TABLE `development_software` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enter_editing`
--

DROP TABLE IF EXISTS `enter_editing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enter_editing` (
  `editing_id` int(11) NOT NULL,
  `actuality_editing` date DEFAULT NULL,
  `contributor_editing` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`editing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enter_editing`
--

LOCK TABLES `enter_editing` WRITE;
/*!40000 ALTER TABLE `enter_editing` DISABLE KEYS */;
/*!40000 ALTER TABLE `enter_editing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enter_source`
--

DROP TABLE IF EXISTS `enter_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enter_source` (
  `source_id` int(11) NOT NULL,
  `url_source` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enter_source`
--

LOCK TABLES `enter_source` WRITE;
/*!40000 ALTER TABLE `enter_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `enter_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `headset_provider`
--

DROP TABLE IF EXISTS `headset_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `headset_provider` (
  `provider_name` varchar(50) NOT NULL,
  `description_provider` text,
  PRIMARY KEY (`provider_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `headset_provider`
--

LOCK TABLES `headset_provider` WRITE;
/*!40000 ALTER TABLE `headset_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `headset_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reference_company`
--

DROP TABLE IF EXISTS `reference_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reference_company` (
  `reference_company_id` int(11) NOT NULL,
  `name_reference_company` varchar(50) DEFAULT NULL,
  `url_reference_company` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`reference_company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reference_company`
--

LOCK TABLES `reference_company` WRITE;
/*!40000 ALTER TABLE `reference_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `reference_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sector`
--

DROP TABLE IF EXISTS `sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sector` (
  `sector_name` varchar(50) NOT NULL,
  `reference_company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`sector_name`),
  KEY `reference_company_id` (`reference_company_id`),
  CONSTRAINT `company_fk_1` FOREIGN KEY (`reference_company_id`) REFERENCES `reference_company` (`reference_company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sector`
--

LOCK TABLES `sector` WRITE;
/*!40000 ALTER TABLE `sector` DISABLE KEYS */;
/*!40000 ALTER TABLE `sector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_feature`
--

DROP TABLE IF EXISTS `type_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_feature` (
  `ftype_name` varchar(50) NOT NULL,
  `description_ftype` text,
  PRIMARY KEY (`ftype_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_feature`
--

LOCK TABLES `type_feature` WRITE;
/*!40000 ALTER TABLE `type_feature` DISABLE KEYS */;
/*!40000 ALTER TABLE `type_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_headset`
--

DROP TABLE IF EXISTS `type_headset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_headset` (
  `htype_name` varchar(50) NOT NULL,
  `description_htype` text,
  PRIMARY KEY (`htype_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_headset`
--

LOCK TABLES `type_headset` WRITE;
/*!40000 ALTER TABLE `type_headset` DISABLE KEYS */;
/*!40000 ALTER TABLE `type_headset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_software`
--

DROP TABLE IF EXISTS `type_software`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_software` (
  `stype_name` varchar(50) NOT NULL,
  `description_stype` text,
  PRIMARY KEY (`stype_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_software`
--

LOCK TABLES `type_software` WRITE;
/*!40000 ALTER TABLE `type_software` DISABLE KEYS */;
/*!40000 ALTER TABLE `type_software` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usecase`
--

DROP TABLE IF EXISTS `usecase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usecase` (
  `usecase_id` int(11) NOT NULL,
  `name_usecase` varchar(50) DEFAULT NULL,
  `description_usecase` text,
  `division_name` varchar(50) DEFAULT NULL,
  `prozess_id` int(11) DEFAULT NULL,
  `sector_name` varchar(50) DEFAULT NULL,
  `b2b_usecase` tinyint(1) DEFAULT NULL,
  `reference_company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`usecase_id`),
  KEY `division_name` (`division_name`),
  KEY `sector_name` (`sector_name`),
  KEY `prozess_id` (`prozess_id`),
  KEY `Company_fk_1_idx` (`reference_company_id`),
  KEY `company_fk_2_idx` (`reference_company_id`),
  CONSTRAINT `company_fk_2` FOREIGN KEY (`reference_company_id`) REFERENCES `reference_company` (`reference_company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `usecase_ibfk_1` FOREIGN KEY (`division_name`) REFERENCES `business_division` (`division_name`),
  CONSTRAINT `usecase_ibfk_3` FOREIGN KEY (`sector_name`) REFERENCES `sector` (`sector_name`),
  CONSTRAINT `usecase_ibfk_4` FOREIGN KEY (`prozess_id`) REFERENCES `business_process` (`prozess_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usecase`
--

LOCK TABLES `usecase` WRITE;
/*!40000 ALTER TABLE `usecase` DISABLE KEYS */;
/*!40000 ALTER TABLE `usecase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usecase_artifact`
--

DROP TABLE IF EXISTS `usecase_artifact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usecase_artifact` (
  `artifact_name` varchar(50) NOT NULL,
  `description_artifact` text,
  `realisation_artifact` tinyint(1) DEFAULT NULL,
  `usecase_id` int(11) DEFAULT NULL,
  `ar_feature_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`artifact_name`),
  KEY `usecase_id` (`usecase_id`),
  KEY `feature_fk_1_idx` (`ar_feature_name`),
  CONSTRAINT `feature_fk_1` FOREIGN KEY (`ar_feature_name`) REFERENCES `ar_feature` (`ar_feature_name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `usecase_fk_1` FOREIGN KEY (`usecase_id`) REFERENCES `usecase` (`usecase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usecase_artifact`
--

LOCK TABLES `usecase_artifact` WRITE;
/*!40000 ALTER TABLE `usecase_artifact` DISABLE KEYS */;
/*!40000 ALTER TABLE `usecase_artifact` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-24 16:04:21
